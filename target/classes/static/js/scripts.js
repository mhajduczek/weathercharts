/*<![CDATA[*/
var spinner = null;

function getWeatherData() {
    var selectedCityId = $('#city option:selected').val();
	$("#city").prop("disabled", true);

    if (selectedCityId != null && selectedCityId != '') {
        $.getJSON("weather?city_id=" + selectedCityId,
            function(receivedData, status){

                var dataT = [];
                var dataH = [];
                var dataP = [];

				for (var i = 0; i < receivedData.cityWeathers.length; i++) {
                    var chartItem = receivedData.cityWeathers[i];

                    var temp = Math.round(chartItem.temp*100)/100;
                    var pressure = Math.round(chartItem.pressure*100)/100;
                    var humidity = Math.round(chartItem.humidity*100)/100;
					var utcTimeInMillis = chartItem.unixTime * 1000;

					dataT.push([utcTimeInMillis, temp]);
                    dataH.push([utcTimeInMillis, humidity]);
                    dataP.push([utcTimeInMillis, pressure]);
                }

                printChart('temp', dataT);
				printChart('humidity', dataH);
				printChart('pressure', dataP);
            }
        ).fail(function() {
			$("#div_temp").empty();
			$("#div_humidity").empty();
			$("#div_pressure").empty();
			$("#info").text("Sorry, something bad happened. Contact with the administrator.");
		}).always(function() {
			$("#city").prop("disabled", false);
		});
    }
}

function printChart(type, seriesData) {
	var seriesName = null;
	var unit = null;
	var seriesColor = null;

	if (type == 'temp') {
		seriesName = 'Temperature';
		unit = '°C';
		seriesColor = 'red';
	} if (type == 'humidity') {
		seriesName = 'Humidity';
		unit = '%';
		seriesColor = 'blue';
	} if (type == 'pressure') {
		seriesName = 'Pressure';
		unit = 'hPa';
		seriesColor = 'green';
	}

	var chart = new Highcharts.Chart({
		chart: {
			renderTo: 'div_' + type,
			type: 'spline',
			zoomType: 'x',
			panning: true,
			panKey: 'shift'
		},
		title: {
			text: NaN,
		},
		xAxis: {
			type: 'datetime'
		},
		yAxis: {
			title: {
				text: seriesName + ' (' + unit + ')'
			},
			plotLines: [{
				value: 0,
				width: 1,
				color: '#808080'
			}]
		},
		tooltip: {
			useHTML: true,
			shared: true,
			formatter: function() {
				var s = '<small>'+ Highcharts.dateFormat('%A, %e %b. %H:%M', this.x) +' UTC</small><table>';
				$.each(this.points, function(i, point) {
						s += '<tr><td style="color:'+point.series.color+'">'+ point.series.name +': </td>'+
						'<td style="text-align: right"><b>'+point.y + unit + '</b></td></tr>';
				});
				return s+'</table>';
			}
		},
		plotOptions: {
            series: {
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                }
            }
        },
		legend: {
			layout: 'vertical',
			align: 'right',
			x: 0,
			verticalAlign: 'top',
			y: 0,
			floating: true,
			backgroundColor: '#FFFFFF'
		},
		series: [{
			name: seriesName,
			data: seriesData,
			color: seriesColor,
			}]
	});
}

function initSpinner() {
	var opts = {
	  lines: 11 // The number of lines to draw
	, length: 28 // The length of each line
	, width: 14 // The line thickness
	, radius: 42 // The radius of the inner circle
	, scale: 0.25 // Scales overall size of the spinner
	, corners: 1 // Corner roundness (0..1)
	, color: '#000' // #rgb or #rrggbb or array of colors
	, opacity: 0.25 // Opacity of the lines
	, rotate: 0 // The rotation offset
	, direction: 1 // 1: clockwise, -1: counterclockwise
	, speed: 1 // Rounds per second
	, trail: 60 // Afterglow percentage
	, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
	, zIndex: 2e9 // The z-index (defaults to 2000000000)
	, className: 'spinner' // The CSS class to assign to the spinner
	, top: '50%' // Top position relative to parent
	, left: '50%' // Left position relative to parent
	, shadow: false // Whether to render a shadow
	, hwaccel: false // Whether to use hardware acceleration
	, position: 'absolute' // Element positioning
	}
	var target = document.getElementById('spin')
	spinner = new Spinner(opts);
}

$.ajaxSetup({
    beforeSend:function(){
        var target = document.getElementById('spin')
		spinner.spin(target);
    },
    complete:function(){
        spinner.stop();
    }
});

$(document).ready(function(){
	initSpinner();

    if (window.location.hash) {
        var fragmentPart = window.location.hash.replace('#','');
        $('#city').val( fragmentPart );
        getWeatherData();
    }

    $("#hint").hide();

    $('#city').change(
        function() {
			$("#info").text("");

            var selectedCityId = $('#city option:selected').val();
            window.location.hash = selectedCityId;

			if (selectedCityId) {
				getWeatherData();
				$("#hint").show();
			} else {
				$("#div_temp").empty();
				$("#div_humidity").empty();
				$("#div_pressure").empty();
				$("#hint").hide();
			}
        }
    );
});
/*]]>*/