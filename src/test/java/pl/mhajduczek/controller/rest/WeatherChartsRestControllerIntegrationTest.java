package pl.mhajduczek.controller.rest;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import pl.mhajduczek.WeatherChartsApplication;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WeatherChartsApplication.class)
@WebAppConfiguration
@IntegrationTest("${server.port=0}")
public class WeatherChartsRestControllerIntegrationTest {

	@Value("${local.server.port}")
	private int port;

	private String appUrl;
	private RestTemplate restTemplate;

	@Before
	public void setup() {
		this.appUrl = "http://localhost:" + port + "/weather?city_id=2643741";
		this.restTemplate = new TestRestTemplate();
	}

	@Test
	public void getWeatherDataForLondon() throws Exception {
		ResponseEntity<String> response =
				this.restTemplate.getForEntity(this.appUrl, String.class);
		assertThat(response.getBody(), both(containsString("city"))
				.and(containsString("id"))
				.and(containsString("name"))
				.and(containsString("country"))
				.and(containsString("cityWeathers"))
				.and(containsString("date"))
				.and(containsString("unixTime"))
				.and(containsString("temp"))
				.and(containsString("pressure"))
				.and(containsString("humidity")));
	}
}
