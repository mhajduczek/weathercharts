package pl.mhajduczek.controller.rest;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.mhajduczek.WeatherChartsApplication;

import static org.hamcrest.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WeatherChartsApplication.class)
@WebAppConfiguration
public class WeatherChartsRestControllerTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void getWeatherDataForLondon() throws Exception {
		this.mockMvc.perform(get("/weather?city_id=2643741").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.city.id").value(2643741))
				.andExpect(jsonPath("$.city.name").value("City of London"))
				.andExpect(jsonPath("$.city.country").value("GB"))
				.andExpect(jsonPath("$.cityWeathers[0].date", any(String.class)))
				.andExpect(jsonPath("$.cityWeathers[0].unixTime", any(Integer.class)))
				.andExpect(jsonPath("$.cityWeathers[0].temp", any(Double.class)))
				.andExpect(jsonPath("$.cityWeathers[0].pressure", any(Double.class)))
				.andExpect(jsonPath("$.cityWeathers[0].humidity", any(Double.class)));
	}
}
