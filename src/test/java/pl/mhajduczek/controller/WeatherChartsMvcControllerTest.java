package pl.mhajduczek.controller;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.mhajduczek.WeatherChartsApplication;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WeatherChartsApplication.class)
@WebAppConfiguration
public class WeatherChartsMvcControllerTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testWeatherChartsMVC() throws Exception {
		this.mockMvc.perform(get("/weatherCharts"))
				.andExpect(status().isOk())
				.andExpect(view().name("weatherCharts"))
				.andExpect(model().attribute("cityList", hasSize(3)))
				.andExpect(model().attribute("cityList", hasItem(
						allOf(
								hasProperty("id", is(2643741L)),
								hasProperty("name", is("City of London, UK"))
						)
				)))
				.andExpect(model().attribute("cityList", hasItem(
						allOf(
								hasProperty("id", is(5128581L)),
								hasProperty("name", is("New York City"))
						)
				)))
				.andExpect(model().attribute("cityList", hasItem(
						allOf(
								hasProperty("id", is(5549222L)),
								hasProperty("name", is("Washington, D.C."))
						)
				)));
	}
}
