package pl.mhajduczek;

import com.google.common.cache.CacheBuilder;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;

import org.slf4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import pl.mhajduczek.service.WeatherDataService;

import java.util.concurrent.TimeUnit;

@EnableCaching
@EnableScheduling
@SpringBootApplication
public class WeatherChartsApplication {

	private static final Logger log = LoggerFactory.getLogger(WeatherDataService.class);

	public static final String CACHE_NAME = "weather_cache";

	@Autowired
	private WeatherDataService weatherDataService;

	@Value("${cache.expireTimeInSeconds}")
	private long cacheExpireInSeconds;

	public static void main(String[] args) {
		SpringApplication.run(WeatherChartsApplication.class, args);
	}

	@Bean(name = "cities")
	public PropertiesFactoryBean mapper() {
		PropertiesFactoryBean bean = new PropertiesFactoryBean();
		bean.setLocation(new ClassPathResource("cities.properties"));
		return bean;
	}

	@Bean
	public CacheManager cacheManager() {
		log.info("Initializing guava cache manager.");

		GuavaCacheManager cacheManager = new GuavaCacheManager(WeatherChartsApplication.CACHE_NAME);
		cacheManager.setCacheBuilder(
				CacheBuilder.
						newBuilder().
						expireAfterWrite(cacheExpireInSeconds, TimeUnit.SECONDS).
						maximumSize(60));
		return cacheManager;
	}
}
