package pl.mhajduczek.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.mhajduczek.WeatherChartsApplication;
import pl.mhajduczek.model.Weather;

/**
 * Created by mh on 2016-03-28.
 */
@Service
public class WeatherDataService {

    private static final Logger log = LoggerFactory.getLogger(WeatherDataService.class);

    @Value("${openweathermap.api-key}")
    private String appId;

    @Value("${openweathermap.api-url}")
    private String apiUrl;

    @Value("${openweathermap.units}")
    private String units;

    @Value("${openweathermap.mode}")
    private String mode;

    @Cacheable(value = WeatherChartsApplication.CACHE_NAME)
    public Weather retrieveData(long cityId) {
        log.debug("Retrieving weather data from OpenWeatherMap REST service for cityId=" + cityId );
        try {
            RestTemplate restTemplate = new RestTemplate();

            Weather weatherData = restTemplate.getForObject(this.apiUrl, Weather.class, cityId, this.units, this.mode, this.appId);
            return weatherData;
        } catch (Exception e) {
            log.error("Error occurred during calling OpenWeatherMap API.", e);
            throw e;
        }
    }
}
