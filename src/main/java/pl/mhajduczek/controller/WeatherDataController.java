package pl.mhajduczek.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.mhajduczek.model.City;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by mh on 2016-03-28.
 */
@Controller
public class WeatherDataController {

    private static final Logger log = LoggerFactory.getLogger(WeatherDataController.class);

    private static final String WEATHER_CHARTS_VIEW = "weatherCharts";

    @Resource(name = "cities")
    private Properties cities;

    @RequestMapping("/weatherCharts")
    public String initPage(Model model) {
        List<City> cityList = new ArrayList<>();
        try {
            for (Object key : this.cities.keySet()) {
                cityList.add(new City(Long.parseLong((String) cities.get(key)), (String) key));
            }
        } catch (Exception e) {
            log.error("WeatherDataController.initPage: Error during initialization of cityList model attribute.", e);
        }
        model.addAttribute("cityList", cityList);
        return WeatherDataController.WEATHER_CHARTS_VIEW;
    }
}
