package pl.mhajduczek.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.mhajduczek.model.Weather;
import pl.mhajduczek.service.WeatherDataService;

/**
 * Created by mh on 2016-03-29.
 */
@RestController
public class WeatherDataRestController {

    private static final Logger log = LoggerFactory.getLogger(WeatherDataRestController.class);

    @Autowired
    WeatherDataService weatherDataService;

    @RequestMapping(value = "/weather", method = RequestMethod.GET)
    public Weather getWeatherData(@RequestParam(name = "city_id", required = true) long cityId) {
        log.debug("Getting weather data for cityId=" + cityId);
        Weather result = null;
        try {
            result = this.weatherDataService.retrieveData(cityId);
            return result;
        } catch (Exception e) {
            log.error("Error occurred while getting weather data for cityId=" + cityId, e);
            throw e;
        } finally {
            log.debug("Got weather data for cityId= " + cityId + ", result= " + result);
        }
    }
}
