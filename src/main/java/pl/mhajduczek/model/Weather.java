package pl.mhajduczek.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import pl.mhajduczek.json.WeatherJsonDeserializer;

import java.util.List;

/**
 * Created by mh on 2016-03-28.
 */
@JsonDeserialize(using = WeatherJsonDeserializer.class)
public class Weather {

    private City city;

    private List<CityWeather> cityWeathers;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<CityWeather> getCityWeathers() {
        return cityWeathers;
    }

    public void setCityWeathers(List<CityWeather> cityWeathers) {
        this.cityWeathers = cityWeathers;
    }

    @Override
    public String toString() {
        return "WeatherData{" +
                "city=" + city +
                ", cityWeathers=" + cityWeathers +
                '}';
    }
}
