package pl.mhajduczek.json;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mhajduczek.model.City;
import pl.mhajduczek.model.CityWeather;
import pl.mhajduczek.model.Weather;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mh on 2016-03-31.
 */
public class WeatherJsonDeserializer extends JsonDeserializer<Weather> {

    private static final Logger log = LoggerFactory.getLogger(WeatherJsonDeserializer.class);

    @Override
    public Weather deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        try {
            Root root = jp.readValueAs(Root.class);

            Weather weather = new Weather();
            weather.setCity(root.city);

            if (root.list != null) {
                List<pl.mhajduczek.model.CityWeather> cityWeathers = new ArrayList<>();

                for (CityWeather cw : root.list) {
                    pl.mhajduczek.model.CityWeather cityWeather = new pl.mhajduczek.model.CityWeather();
                    cityWeather.setDate(cw.date);
                    cityWeather.setUnixTime(cw.unixTime);
                    if (cw.main != null) {
                        cityWeather.setTemp(cw.main.temp);
                        cityWeather.setHumidity(cw.main.humidity);
                        cityWeather.setPressure(cw.main.pressure);
                    }
                    cityWeathers.add(cityWeather);
                }

                weather.setCityWeathers(cityWeathers);
            }

            return weather;
        } catch (Exception e) {
            log.error("WeatherJsonDeserializer: exception occured during JSON parsing.", e);
            throw e;
        }
    }

    private static class Root {
        public City city;
        public List<CityWeather> list;
    }

    public static class CityWeather {
        @JsonProperty(value = "dt_txt")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        public Date date;

        @JsonProperty(value = "dt")
        public long unixTime;

        public Main main;
    }


    private static class Main {
        public double temp;
        public double pressure;
        public double humidity;
    }
}
